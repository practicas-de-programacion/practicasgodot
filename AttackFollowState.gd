class_name AttackFollowState
extends State
 
@export var actor: CharacterBody2D

var hitSound = preload("res://Sounds/Sound effects/sword-sound-2-36274.mp3")

func Enter():
	
	%AudioPlayer.stream = hitSound
	%AudioPlayer.play()

func Physics_update(_delta):
	
	actor.mDamage = 2
	%HurtBox.disabled = false
	if actor.is_on_floor():
		actor.velocity.x = 0
	
	
	%AnimatedSprite2D.animation = "Atack Down"	
		
	if not %AnimatedSprite2D.is_playing() && Input.is_key_pressed(KEY_J):
		transitioned.emit("AttackState")		
		%AnimatedSprite2D.play()
		%HurtBox.disabled = true;
	elif not %AnimatedSprite2D.is_playing():
		transitioned.emit("IdleState")	
		%HurtBox.disabled = true;

