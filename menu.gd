extends CanvasLayer

signal start_game
signal exit_game

# Called when the node enters the scene tree for the first time.
func _ready():
	#$Start.grab_focus()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if Input.is_key_pressed(KEY_ENTER):
		start_game.emit()
	if Input.is_key_pressed(KEY_ESCAPE):	
		exit_game.emit()
	pass


func _on_start_pressed():
	start_game.emit()
	pass # Replace with function body.


func _on_exit_pressed():
	exit_game.emit()
	pass # Replace with function body.
