class_name AirState
extends State
 
@export var actor: CharacterBody2D



func Physics_update(_delta):

	if(actor.is_on_floor()):
		transitioned.emit("IdleState")	
		
	if(actor.velocity.y<-actor.JUMP_VELOCITY/2):
		%AnimatedSprite2D.animation = "JumpStart"
	elif(actor.velocity.y<0 && actor.velocity.y > -actor.JUMP_VELOCITY/2):	
		%AnimatedSprite2D.animation = "JumpMid"
	else:
		%AnimatedSprite2D.animation = "JumpEnd"	
	
	if actor.is_on_wall_only() && Input.is_key_pressed(KEY_SPACE) && actor.velocity.y > 0:
		transitioned.emit("WallSlideState");	
	
	if Input.is_key_pressed(KEY_A):
			actor.velocity.x = 0
			actor.velocity.x -= actor.SPEED		
			%AnimatedSprite2D.flip_h = true
			%HurtBox.position = Vector2(-17,0)
	if Input.is_key_pressed(KEY_D):			
			actor.velocity.x = 0
			%AnimatedSprite2D.flip_h = false
			%HurtBox.position = Vector2(17,0)
			actor.velocity.x += actor.SPEED		
	if Input.is_key_pressed(KEY_J):
		transitioned.emit("AttackState");	
	if Input.is_key_pressed(KEY_K):
		if(actor.canSpecial):
			transitioned.emit("SpecialAtackState");	
					
	%AnimatedSprite2D.play()

