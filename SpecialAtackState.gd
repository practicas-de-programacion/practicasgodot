class_name SpecialAttackState
extends State
 
@export var actor: CharacterBody2D
@export var proyectil: PackedScene

var hitSound = preload("res://Sounds/Sound effects/axe-slash-1-106748.mp3")


func Enter():

	%AudioPlayer.stream = hitSound
	%AudioPlayer.play()
	var pr = proyectil.instantiate()
	pr.position = actor.position;
	actor.get_tree().root.add_child(pr)
	pr.setDir(%AnimatedSprite2D.flip_h)
	$CD.start()
	actor.canSpecial = false
	
func Physics_update(_delta):
	
	if actor.is_on_floor():
		actor.velocity.x = 0;
	
	%AnimatedSprite2D.animation = "SpecialBlue"	
	
	if not %AnimatedSprite2D.is_playing():
		transitioned.emit("IdleState")	
		


func _on_cd_timeout():
	actor.canSpecial = true
	pass # Replace with function body.
