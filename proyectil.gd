extends Node2D


@export var dmgNum: PackedScene

var mDirection = 1
var mVel = 200
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func setDir(dir):
	if(dir):
		mDirection = 1
	else:
		mDirection = -1

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if(mDirection != 1):
		position.x += mVel*delta
		$Sprite2D.flip_h = false;
	else:
		position.x -= mVel*delta
		$Sprite2D.flip_h = true;
	pass


func _on_area_2d_body_entered(body):
	if body.is_in_group("Mob"):
		if(body.getCanDamage()):
			body.OnTakeAnyDamage(5, $Sprite2D.flip_h)		 
			print(body.getCanDamage())		
			var dmg = dmgNum.instantiate()
			body.get_tree().root.add_child(dmg)
			dmg.update_score(5, body.position)
	
	if !body.is_in_group("Player"):
		queue_free()	
	pass # Replace with function body.
