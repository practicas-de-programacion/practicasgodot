class_name IdleState
extends State
 
@export var actor: CharacterBody2D
 
func Enter():
	actor.velocity.x = 0

func Physics_update(_delta):
	
	
	
	%AnimatedSprite2D.animation = "Idle"
	%AnimatedSprite2D.play()


	if Input.is_key_pressed(KEY_A):
		transitioned.emit("WalkState")
	if Input.is_key_pressed(KEY_D):
		transitioned.emit("WalkState")
	if Input.is_key_pressed(KEY_SPACE) && actor.is_on_floor():
		transitioned.emit("JumpState")	
	if(actor.velocity.y > 0):
		transitioned.emit("AirState");	
	if Input.is_key_pressed(KEY_S) && actor.is_on_floor():
		transitioned.emit("CrouchState")
		
	if Input.is_key_pressed(KEY_J):
		transitioned.emit("AttackState");	
	if Input.is_key_pressed(KEY_K):
		if(actor.canSpecial):
			transitioned.emit("SpecialAtackState");	
	if Input.is_key_pressed(KEY_L):
		if(actor.canSlide):
			transitioned.emit("SlideState");	
		
	var res = actor.test_move(actor.transform, Vector2.ZERO)	
	if(res):
		transitioned.emit("CrouchState")		
