extends CanvasLayer

var points

# Called when the node enters the scene tree for the first time.
func _ready():
	
	points = 0
	change_Points(points)
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func set_Hp(hp):
	
	for i in hp:
		
		var heart = TextureRect.new()
		heart.texture = load("res://Legacy-Fantasy - High Forest 2.3/Character/Heart/Heart.png")
		heart.expand_mode = TextureRect.EXPAND_FIT_WIDTH
		$GUI/sections.add_child(heart)

func playerDied():
	$Label.show()
	
func change_Hp(hp):	
	$GUI/sections.get_child(0).queue_free()
	
func change_Points(point):	
	points += point
	$Points.text = str(points)

	
func update_score(hp):
	
	pass

