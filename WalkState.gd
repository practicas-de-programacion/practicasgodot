class_name WalkState
extends State
 
@export var actor: CharacterBody2D
var walkSound = preload("res://Sounds/Sound effects/03_Step_grass (2).mp3")

func Enter():
	
	%AudioPlayer.stream = walkSound
	
func Physics_update(_delta):
	
	
	actor.velocity.x = 0
	if Input.is_key_pressed(KEY_A):
			actor.velocity.x -= actor.SPEED
			if(actor.velocity.y == 0):
				%AnimatedSprite2D.animation = "Walk"
				if !%AudioPlayer.is_playing():
					%AudioPlayer.play()
			%AnimatedSprite2D.flip_h = true
			%HurtBox.position = Vector2(-17,0)
	if Input.is_key_pressed(KEY_D):
			if(actor.velocity.y == 0):
				%AnimatedSprite2D.animation = "Walk"
				if !%AudioPlayer.is_playing():
					%AudioPlayer.play()
			%AnimatedSprite2D.flip_h = false
			%HurtBox.position = Vector2(17,0)
			actor.velocity.x += actor.SPEED
	if Input.is_key_pressed(KEY_SPACE) && actor.is_on_floor():
		transitioned.emit("JumpState")	
	if Input.is_key_pressed(KEY_S) && actor.is_on_floor():
		transitioned.emit("CrouchState")
	
	%AnimatedSprite2D.play()
	
	if Input.is_key_pressed(KEY_J):
		transitioned.emit("AttackState");	
	if Input.is_key_pressed(KEY_K):
		if(actor.canSpecial):
			transitioned.emit("SpecialAtackState");		
	if Input.is_key_pressed(KEY_L):
		if(actor.canSlide):
			transitioned.emit("SlideState");			
		
	if(actor.velocity.x == 0):
		transitioned.emit("IdleState")	

