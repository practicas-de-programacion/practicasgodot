extends CharacterBody2D

var flag
var HP
var canDamage = true
# Called when the node enters the scene tree for the first time.
func _ready():
	flag = true
	$Patrol.start()
	HP = 5
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta_):
	if(canDamage):
		velocity.x = 0
		if(flag):
			$AnimatedSprite2D.animation = "Walk"
			$AnimatedSprite2D.flip_h = false		
			velocity.x -= 80
			#linear_velocity = Vector2(80.0,0.0)
		else:
			$AnimatedSprite2D.animation = "Walk"
			$AnimatedSprite2D.flip_h = true
			velocity.x += 80
	
	if not is_on_floor():
		velocity.y += 500 * delta_
	
		#linear_velocity = Vector2(-80.0,0.0)	
		
	var colisions = move_and_slide()
	$AnimatedSprite2D.play()
	pass


func _on_patrol_timeout():
	flag = !flag
	
	pass # Replace with function body.

func OnTakeAnyDamage(dmg, dirBool):
	if(canDamage):
		$AnimatedSprite2D.animation = "Damaged"
		canDamage = false
		HP-=dmg
		velocity.y -= 90
		if(dirBool):
			velocity.x = -10
		else:
			velocity.x = 10
		$IFrames.start()
		if(HP <= 0):
			queue_free() 
	pass


func _on_i_frames_timeout():
	$AnimatedSprite2D.animation = "Walk"
	canDamage = true
	pass # Replace with function body.

func getCanDamage():
	return canDamage
