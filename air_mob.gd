extends CharacterBody2D


const mVelocity = 100

var flag
var HP
var canDamage = true
var target
# Called when the node enters the scene tree for the first time.
func _ready():
	flag = true
	$Patrol.start()
	HP = 2
	target = null
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta_):
	
	if(canDamage):
		velocity.x = 0
		velocity.y = 0
		
		if(target != null):
			velocity = global_position.direction_to(target.global_position)*(mVelocity+20)
			#global_position.move_toward(target.global_position, delta_*mVelocity)
		
		elif(flag):
			$AnimatedSprite2D.animation = "Fly"
			$AnimatedSprite2D.flip_h = false		
			velocity.x -= mVelocity
			#linear_velocity = Vector2(80.0,0.0)
		else:
			$AnimatedSprite2D.animation = "Fly"
			$AnimatedSprite2D.flip_h = true
			velocity.x += mVelocity
	
		#linear_velocity = Vector2(-80.0,0.0)	
		
	var colisions = move_and_slide()
	$AnimatedSprite2D.play()
	pass


func _on_patrol_timeout():
	flag = !flag
	
	pass # Replace with function body.

func OnTakeAnyDamage(dmg, dirBool):
	if(canDamage):
		$AnimatedSprite2D.animation = "Damaged"
		canDamage = false
		HP-=dmg
		if(dirBool):
			velocity.x = -300
		else:
			velocity.x = 300
		$IFrames.start()
		if(HP <= 0):
			queue_free() 
	pass


func _on_i_frames_timeout():
	$AnimatedSprite2D.animation = "Walk"
	canDamage = true
	pass # Replace with function body.

func getCanDamage():
	return canDamage


func _on_area_2d_body_entered(body):
	if body.is_in_group("Player"):
		target = body
	pass # Replace with function body.


func _on_area_2d_body_exited(body):
	if body.is_in_group("Player"):
		target = null
	pass # Replace with function body.
