class_name JumpState
extends State
 
@export var actor: CharacterBody2D
 
func Physics_update(_delta):
	
	if(actor.is_on_floor() && Input.is_key_pressed(KEY_S)):
		actor.position.y += 5	
		transitioned.emit("IdleState")	
	elif(actor.is_on_floor()):
		actor.velocity.y -= actor.JUMP_VELOCITY	
		transitioned.emit("AirState")	
		
	#Aqui codigo en caso de doble salto
