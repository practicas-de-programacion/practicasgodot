class_name CrouchState
extends State
 
@export var actor: CharacterBody2D
 
func Enter():
	
	%Colision.disabled = true;
	%RollCol.disabled = false;	

func Exit() -> void:
	%Colision.disabled = false;
	%RollCol.disabled = true;	
	

func Physics_update(_delta):
	

	
	
	actor.velocity.x = 0
	if Input.is_key_pressed(KEY_A):
		actor.velocity.x -= actor.SPEED/2
		%AnimatedSprite2D.flip_h = true
		%AnimatedSprite2D.animation = "CrouchWalk"		
	elif Input.is_key_pressed(KEY_D):
		%AnimatedSprite2D.flip_h = false
		actor.velocity.x += actor.SPEED/2
		%AnimatedSprite2D.animation = "CrouchWalk"
	else:
		%AnimatedSprite2D.animation = "Crouch"
	
	if Input.is_key_pressed(KEY_SPACE) && actor.is_on_floor():
		var res = actor.test_move(actor.transform, Vector2(0, 80))	#Asegurarse que el personaje cabe
		if(!res):
			transitioned.emit("JumpState")	
		
	%AnimatedSprite2D.play()
		
	
	if(actor.velocity.y != 0):	
		transitioned.emit("AirState");
		
	if Input.is_key_pressed(KEY_L):
		if(actor.canSlide):
			transitioned.emit("SlideState");			
			
	if !Input.is_key_pressed(KEY_S):
		var res = actor.test_move(actor.transform, Vector2(0, -10))	
		if(!res):
			transitioned.emit("IdleState")	

