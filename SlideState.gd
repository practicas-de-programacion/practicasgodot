class_name SlideState
extends State
 
@export var actor: CharacterBody2D
 
const changeSize = 0
const originalSize = 1
const offset = 13



func Exit() -> void:
	%Colision.disabled = false;
	%RollCol.disabled = true;	
	

func Enter():
	actor.canSlide = false
	$SlideWindow.start()
	$SlideCD.start()
	%Colision.disabled = true;
	%RollCol.disabled = false;	


func Physics_update(_delta):
	actor.velocity.x = 0
	%AnimatedSprite2D.animation = "Slide"
	
	if(%AnimatedSprite2D.flip_h == true):
		actor.velocity.x -= actor.SPEED*2
		if Input.is_key_pressed(KEY_D):
			transitioned.emit("WalkState")
	else:	
		actor.velocity.x += actor.SPEED*2
		if Input.is_key_pressed(KEY_A):
			transitioned.emit("WalkState")
			
			
	if Input.is_key_pressed(KEY_SPACE) && actor.is_on_floor():
		transitioned.emit("JumpState")	
	if(actor.velocity.y > 0):
		transitioned.emit("AirState");
	if Input.is_key_pressed(KEY_J):
		transitioned.emit("AttackState");	

func _on_slide_window_timeout():
	if(actor.hp > 0):
		if(actor.velocity.y != 0):
			transitioned.emit("AirState")
		else:	
			transitioned.emit("IdleState")	
	pass # Replace with function body.


func _on_slide_cd_timeout():
	actor.canSlide = true;
	pass # Replace with function body.
	
