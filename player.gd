extends CharacterBody2D

@export var dmgNum: PackedScene
signal dead
signal hit

const SPEED = 350.0
const JUMP_VELOCITY = 450.0
const AIR_FRICTION = 10

var hp = 3
var mDamage = 1
var canSlide = true
var canDamage = true
var canSpecial = true
var canInteract = false

func _ready():
	$HUD.set_Hp(hp)
	%HurtBox.disabled = true
	up_direction = Vector2(0, -1)
	
func _physics_process(delta):
	
	if position.y > 100:
		position.y = 0 
		position.x = 0
		takeDamage()
		
	if not is_on_floor():
		
		if $PlayerStateMachine.current_state.name != "HitState":	
			if(%AnimatedSprite2D.flip_h == true):
				velocity.x = min(velocity.x + AIR_FRICTION, 0)
			else:
				velocity.x = max(velocity.x - AIR_FRICTION, 0) 
		
		velocity.y += 1000 * delta
			
	var colisions = move_and_slide()
	#for i in get_slide_collision_count():
		#var collision = get_slide_collision(i)
		#print(collision.get_collider().name)
		
	if canInteract && Input.is_key_pressed(KEY_E):
		var interactuables = $HitBoxArea.get_overlapping_areas()
		interactuables[0].interact()
		
		
		
func _on_area_2d_body_entered(body):
	if body.is_in_group("Mob"):
		if(body.getCanDamage()):
			body.OnTakeAnyDamage(mDamage, %AnimatedSprite2D.flip_h)		 			
			var dmg = dmgNum.instantiate()
			body.get_tree().root.add_child(dmg)
			dmg.update_score(mDamage, body.position)			
	pass


func _on_hit_box_area_body_entered(body):
	if body.is_in_group("Mob"):
		if(canDamage):		
			takeDamage()					
	pass # Replace with function body.

func takeDamage():
	hit.emit()
	var dmg = dmgNum.instantiate()
	get_tree().root.add_child(dmg)	
	dmg.update_score(mDamage, position)
	canDamage = false
	hp-=1
	$HUD.change_Hp(hp)
	if(hp<=0):
		$HUD.playerDied()
		dead.emit()
	else:
		$IFrames.start()
		
func _on_i_frames_timeout():
	canDamage = true
	pass # Replace with function body.

func _on_hit_box_area_area_entered(area):
	if	area.is_in_group("Interact"):	
		$Label.show()
		canInteract = true
	if area.is_in_group("Coin"):	
		$HUD.change_Points(100)	
	pass # Replace with function body.


func _on_hit_box_area_area_exited(area):
	if	area.is_in_group("Interact"):	
		$Label.hide()
		canInteract = false
	pass # Replace with function body.
