extends RigidBody2D


# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func update_score(dmg, pos):
	
	$DamageNumber.text = str(dmg)
	if(dmg > 1 && dmg < 4):
		$DamageNumber.add_theme_color_override("font_color", 	Color(1,0,0))
	elif(dmg > 4):	
		$DamageNumber.add_theme_color_override("font_color", 	Color(1,0,1))
	else:
		$DamageNumber.add_theme_color_override("font_color", 	Color(1,1,0))
	position = Vector2(pos.x, pos.y-20)
	#linear_velocity.y = 10;
	$Despawn.start()



func _on_despawn_timeout():
	queue_free()
	pass # Replace with function body.
