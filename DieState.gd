class_name DieState
extends State
 
@export var actor: CharacterBody2D
 
func Enter():
	
	%AnimatedSprite2D.animation = "Die"
	%AnimatedSprite2D.play()
	actor.velocity.x = 0
	

