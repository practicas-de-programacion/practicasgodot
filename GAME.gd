extends Node2D

var playerDead = false
var currentScene
var mPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if(playerDead):
		if Input.is_key_pressed(KEY_R):
			currentScene.queue_free()
			mPlayer.queue_free()
			loadFirstLevel()
	
	pass


func _on_menu_exit_game():
	get_tree().quit()
	pass # Replace with function body.


func _on_menu_start_game():
	
	$Menu.queue_free()
	loadFirstLevel()
	pass # Replace with function body.

func loadFirstLevel():
	currentScene = preload("res://lvl_1.tscn").instantiate()
	mPlayer = preload("res://player.tscn").instantiate()
	get_tree().root.add_child(currentScene)
	get_tree().root.add_child(mPlayer)
	mPlayer.position = Vector2(10,-30)
	mPlayer.dead.connect(_on_player_dead)
	
func loadLevel(newLevel: String):	
	currentScene.queue_free()
	currentScene = load(newLevel).instantiate()
	get_tree().root.add_child(currentScene)
	mPlayer.position = Vector2(0,0)
	
func _on_player_dead():
	playerDead = true
	pass # Replace with function body.
