class_name WallSlideState
extends State
 
@export var actor: CharacterBody2D



func Physics_update(_delta):

	if(actor.is_on_floor()):
		transitioned.emit("IdleState")	
		
	if actor.is_on_wall_only() && actor.velocity.y > 0 && Input.is_key_pressed(KEY_SPACE):		
		actor.velocity.y = 50 * _delta		
		%AnimatedSprite2D.animation = "WallSlide"
		
	elif(actor.velocity.y == 0):
		transitioned.emit("IdleState")	
	else:
		transitioned.emit("AirState")	
		
	if Input.is_key_pressed(KEY_A):
			actor.velocity.x = 0
			actor.velocity.x -= actor.SPEED		
			%AnimatedSprite2D.flip_h = true
			%HurtBox.position = Vector2(-17,0)
	if Input.is_key_pressed(KEY_D):			
			actor.velocity.x = 0
			%AnimatedSprite2D.flip_h = false
			%HurtBox.position = Vector2(17,0)
			actor.velocity.x += actor.SPEED		

	%AnimatedSprite2D.play()

