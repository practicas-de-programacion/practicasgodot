class_name HitState
extends State
 
@export var actor: CharacterBody2D
 
func Enter():
	
	%AnimatedSprite2D.animation = "Hit"
	%AnimatedSprite2D.play()
	
	actor.velocity.y -= 100	
	
func Physics_update(_delta):	
	actor.velocity.x = 0
	if(%AnimatedSprite2D.flip_h):
		actor.velocity.x += actor.SPEED
	else:
		actor.velocity.x -= actor.SPEED

func _on_i_frames_timeout():
	transitioned.emit("IdleState")	
	pass # Replace with function body.
